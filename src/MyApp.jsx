import React from "react";
import MyRoutes from "./practiceComponent/MyRoutes";
import ReactRouter from "./practiceComponent/ReactRouter";

const MyApp = () => {
  return (
    <div>
      <ReactRouter></ReactRouter>
    </div>
  );
};

export default MyApp;
