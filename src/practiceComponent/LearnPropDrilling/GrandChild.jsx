import React, { useContext } from "react";
import { Context1, Context2 } from "../../App";

const GrandChild = () => {
  let value = useContext(Context1);

  let value1 = useContext(Context2);

  console.log(value);

  return (
    <div>
      GrandChild
      <br></br>
      name is {value.name}
      <br></br>
      age is {value.age}
      <br></br>
      address is {value1.address}
      <button
        onClick={() => {
          value.setName("hari");
        }}
      >
        Change name
      </button>
    </div>
  );
};

export default GrandChild;
