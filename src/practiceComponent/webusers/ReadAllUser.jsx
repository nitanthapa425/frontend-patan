import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const ReadAllUser = () => {
  let [users, setUser] = useState([]);
  let navigate = useNavigate();

  let getAllUsers = async () => {
    let result = await axios({
      url: "http://localhost:8000/web-users",
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    });

    setUser(result.data.data);
  };

  useEffect(() => {
    getAllUsers();
  }, []);

  return (
    <div>
      {users.map((item, i) => {
        console.log(item);
        return (
          <div
            key={i}
            style={{ border: "solid red 3px", marginBottom: "20px" }}
          >
            <p> email is {item.email}</p>
            <p> full name {item.fullName}</p>
            <p> gender is {item.gender}</p>
            <button
              style={{ marginRight: "30px" }}
              onClick={() => {
                navigate(`/admin/${item._id}`);
              }}
            >
              View
            </button>
            <button
              style={{ marginRight: "30px" }}
              onClick={(e) => {
                navigate(`/admin/update/${item._id}`);
              }}
            >
              Edit
            </button>
            <button
              style={{ marginRight: "30px" }}
              onClick={async () => {
                let result = await axios({
                  url: `http://localhost:8000/web-users/${item._id}`,
                  method: "DELETE",
                  headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`,
                  },
                });

                getAllUsers();
              }}
            >
              Delete
            </button>
          </div>
        );
      })}
    </div>
  );
};

export default ReadAllUser;

/* 

get all users
url: "http://localhost:8000/users"
method:"GET"


get specific users
url: "http://localhost:8000/users/1234214"
method:"GET"

*/
