import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";

const AdminForgotPassword = () => {
  let [email, setEmail] = useState("");

  let navigate = useNavigate();
  let token = localStorage.getItem("token");

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      email: email,
    };

    try {
      let result = await axios({
        url: `http://localhost:8000/web-users/forgot-password`,
        method: "POST",
        data: data,
      });

      setEmail("");

      toast.success("Link has been set to your email to reset password.");

      //   localStorage.removeItem("token");
      //   navigate("/admin/login");
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  return (
    <div>
      <ToastContainer></ToastContainer>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="email">Email: </label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
        </div>

        <button type="submit">Forgot Password</button>
      </form>
    </div>
  );
};

export default AdminForgotPassword;
