import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

const ReadSpecificUser = () => {
  let token = localStorage.getItem("token");
  let [user, setUser] = useState({});
  let navigate = useNavigate();
  let params = useParams();
  let id = params.id;
  let getAdminUser = async () => {
    try {
      let result = await axios({
        // url: "http://localhost:8000/web-users/my-user",
        url: `http://localhost:8000/web-users/${id}`,
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      setUser(result.data.data);
    } catch (error) {}
  };

  useEffect(() => {
    getAdminUser();
  }, []);
  return (
    <div>
      <p> Full name = {user.fullName}</p>
      <p> Gender= {user.gender}</p>
      <p> Date of birth = {new Date(user.dob).toLocaleDateString()}</p>
      <p> email = {user.email}</p>
      <p> role = {user.role}</p>

      <button
        onClick={() => {
          navigate("/admin/user-update");
        }}
      >
        Update User
      </button>
    </div>
  );
};

export default ReadSpecificUser;
