import React from "react";

const LearnMap1 = () => {
  let names = ["nitan", "ram", "hari"];

  let printNames = () => {
    let list = names.map((item, i) => {
      return <div>my best friend is {item}</div>;
    });
    return list;
  };
  
  return <div>{printNames()}</div>;
};

export default LearnMap1;
