import React from "react";
import { products } from "../product";

// {
//     id: 1,
//     title: "MacBook Pro",
//     category: "Laptops",
//     price: 100000.0,
//     description: "A high-performance laptop.",
//     manufactureDate: "2023-05-15T08:30:00",
//     isAvailable: true,
//   },

const LearnMap2 = () => {
  let task1 = () => {
    let list = products.map((item, i) => {
      return <div>{item.title}</div>;
    });

    return list;
  };

  let task2 = () => {
    let list = products.map((item, i) => {
      return (
        <div key={i}>
          {item.title} costs NRs. {item.price} and its category is{" "}
          {item.category}
        </div>
      );
    });

    return list;
  };

  let task3 = () => {
    let list = products
      .filter((item, i) => {
        if (item.price > 2000) {
          return true;
        }
      })
      .map((item, i) => {
        return (
          <div key={i}>
            {item.title} costs NRs. {item.price} and its category is{" "}
            {item.category}
          </div>
        );
      });

    return list;
  };

  return (
    <div>
      <h1>The Products in our Shop are</h1>
      {/* {task1()} */}

      {/* {task2()} */}

      {task3()}
    </div>
  );
};

export default LearnMap2;
