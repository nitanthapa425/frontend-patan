import React from "react";
import { Route, Routes } from "react-router-dom";
import CreateProduct from "./product/CreateProduct";
import ReadAllProduct from "./product/ReadAllProduct";
import CreateStudent from "./student/CreateStudent";
import ReadAllStudent from "./student/ReadAllStudent";
import ReadSpecificProduct from "./product/ReadSpecificProduct";

const MyRoutes = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<div> this is home page</div>}></Route>
        <Route
          path="/products/:id"
          element={<ReadSpecificProduct></ReadSpecificProduct>}
        ></Route>
        <Route
          path="/products/create"
          element={<CreateProduct></CreateProduct>}
        ></Route>
        <Route
          path="/products"
          element={<ReadAllProduct></ReadAllProduct>}
        ></Route>
        <Route
          path="/students/create"
          element={<CreateStudent></CreateStudent>}
        ></Route>
        <Route
          path="/students"
          element={<ReadAllStudent></ReadAllStudent>}
        ></Route>

        <Route path="/*" element={<div>404 page</div>}></Route>
      </Routes>
    </div>
  );
};

export default MyRoutes;

/* 
 "http://localhost:3000/products/create" => This is products Form
 "http://localhost:3000/products"  => Product Listing page

 "http://localhost:3000/students/create"  = This is student Form

 "http://localhost:3000/students" => Student Listing page

*/
