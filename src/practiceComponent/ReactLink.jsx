import React, { useState } from "react";
import { NavLink } from "react-router-dom";

const ReactLink = () => {
  return (
    <div>
      {/* <a href="/products/create">Create Productsaaa</a> */}
      <br></br>

      <NavLink to="/products/create" style={{ marginRight: "20px" }}>
        Create Products
      </NavLink>
      <NavLink to="/products" style={{ marginRight: "20px" }}>
        Products
      </NavLink>
      <NavLink to="/students/create" style={{ marginRight: "20px" }}>
        Create Students
      </NavLink>
      <NavLink to="/students" style={{ marginRight: "20px" }}>
        Students
      </NavLink>
      <NavLink to="/admin/register" style={{ marginRight: "20px" }}>
        Admin Register
      </NavLink>
      <NavLink to="/admin/login" style={{ marginRight: "20px" }}>
        Admin Login
      </NavLink>

      <NavLink
        to="/admin/my-profile"
        style={{ marginRight: "20px", fontWeight: "bold" }}
      >
        My Profile
      </NavLink>

      <NavLink
        to="/admin/logout"
        style={{ marginRight: "20px" }}
        //  onClick = {()=>{
        //   window.location.reload(true);
        //  }}
        onClick={() => {
          window.location.reload(true);
        }}
      >
        Logout
      </NavLink>
      <NavLink to="/admin/update-password" style={{ marginRight: "20px" }}>
        Update Password
      </NavLink>
      <NavLink to="/admin/read-all-user" style={{ marginRight: "20px" }}>
        Read All User
      </NavLink>
    </div>
  );
};

export default ReactLink;
