import React, { useState } from "react";

const Form2 = () => {
  let onSubmit = (e) => {
    let data = {
      isMarried: isMarried,
    };
    console.log(data);
    e.preventDefault();
  };

  let [isMarried, setIsMarried] = useState(false);
  return (
    <div>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="isMarried">Is Married: </label>
          <input
            type="checkbox"
            id="isMarried"
            // value={isMarried}
            checked={isMarried === true}
            onChange={(e) => {
              setIsMarried(e.target.checked);
            }}
          ></input>
        </div>
        <button type="submit">Proceed</button>
      </form>
    </div>
  );
};

export default Form2;

/* 
isMarried
*/
