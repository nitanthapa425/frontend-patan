import React from "react";

const LearnTearnary = () => {
  let age = 30;
  return (
    <div>
      {
        age<18?<div>Underage</div>
        :age>=18&&age<=60?<div>Adult</div>
        :age>=60&&age<=150?<div>Old</div>
        :null
      }
    </div>
  );
};

export default LearnTearnary;
