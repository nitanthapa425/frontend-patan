import React, { useEffect, useState } from "react";

const LearnUseEffectHook = () => {
  let [count, setCount] = useState(0);
  let [count2, setCount2] = useState(100);

  useEffect(() => {
    console.log("i am useEffect function");
  }, [count, count2]);
  //it execute for 1st render
  // from 2nd render the fun will only execute if count or count2 variable is changed

  useEffect(() => {
    console.log("i am useEffect 2");
  }, [count]);
  //it execute for 1st render
  // from 2nd render the fun will only execute if count  variable is changed
  useEffect(() => {
    console.log("i am useEffect 3");
  }, []);
  // it execute for first render only

  useEffect(() => {
    console.log("i am useEffect 4");
  });
  // it execute for each render

  return (
    <div>
      count is {count}
      <br />
      <button
        onClick={(e) => {
          setCount(count + 1);
        }}
      >
        Increment
      </button>
      <br></br>
      count2 is {count2}
      <br />
      <button
        onClick={(e) => {
          setCount2(count2 + 1);
        }}
      >
        Increment count 2
      </button>
    </div>
  );
};

export default LearnUseEffectHook;
