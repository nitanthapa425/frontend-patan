import React, { useEffect, useState } from "react";

const LearnCleanUpFunction = () => {
  let [count, setCount] = useState(0);

  console.log("***********************");

  useEffect(() => {
    console.log("i am useEffect");

    return () => {
      console.log("i am clean up function");
    };
  }, [count]);

  /*
  cleanup function are those function which is return by useEffect 
  cleanup function does not execute in first render
  but from second render the clean up function will execute if useEffect fuc run


  What Happen when use Effect fun gets execute
  first clean up fun will run the the code above it will execute

  when component is unmount nothing gets executed except cleanup function
   */
  return (
    <div>
      count1 is {count}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1);
        }}
      >
        Increment Count1
      </button>
    </div>
  );
};

export default LearnCleanUpFunction;
